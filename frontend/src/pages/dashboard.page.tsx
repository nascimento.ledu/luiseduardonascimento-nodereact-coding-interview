import React, { FC, useState, useEffect } from "react";
import { navigate, RouteComponentProps, useLocation } from "@reach/router";
import { parse } from "query-string"

import { IUserProps, IApiResponse } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import { SearchPeople } from "../components/searchPeople";
import { Pagination } from "../components/pagination";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [usersTotal, setUsersTotal] = useState<number>(0);

  const location = useLocation();

  const getUsers = async (page: string): Promise<void> => {
    let result: IApiResponse;
    setLoading(true);

    result = await backendClient.getAllUsers(`?page=${page}`);

    const path = `${location.origin}?page=${result.data.page}`;
    navigate(path);

    setUsers(result.data.users);
    setUsersTotal(result.data.totalItems);
    setCurrentPage(result.data.page);
    setLoading(false);
  };

  const getUsersByFilter = async (searchValueDebounced: string, page: string): Promise<void> => {
    let result: IApiResponse;
    setLoading(true);

    result = await backendClient.getByFilter(`?search_term=${searchValueDebounced}&page=${page}`);

    const path = `${location.origin}?search_term=${searchValueDebounced}&page=${page}`;
    navigate(path);

    setUsers(result.data.users);
    setUsersTotal(result.data.totalItems);
    setCurrentPage(result.data.page);
    setLoading(false);
  };

  const handleChangePage = (page: number): void => {
    if (location.search.indexOf('search_term') > -1) {
      const searchTerm = parse(location.search).search_term?.toString() || '';
      getUsersByFilter(searchTerm, page.toString());
    } else {
      getUsers(page.toString());
    }
  };

  useEffect(() => {
    const page = parse(location.search).page || 1;
    handleChangePage(Number(page));
  }, []);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div
          style={{
            margin: "22px",
            width: "100%",
          }}
        >
          <SearchPeople getUsersByFilter={getUsersByFilter} handleGetAllUsers={getUsers} />
          <div
            style={{
              position: "relative",
            }}
          >
            {loading && (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  position: "absolute",
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  backgroundColor: "#FFF",
                  zIndex: 99,
                }}
              >
                <CircularProgress size={60} />
              </div>
            )}
            {users.length && !loading
              ? <div>
                  <div
                    style={{
                      display: "flex",
                      flexWrap: "wrap",
                      justifyContent: "center",
                      position: "relative",
                      height: "100vh",
                      marginBottom: "56px",
                    }}
                  >
                    {users.map((user) => {
                      return <UserCard key={user.id} {...user} />;
                    })}
                  </div>
                  <Pagination
                    currentPage={currentPage}
                    itemsPerPage={12}
                    totalItems={usersTotal}
                    handlePageChange={handleChangePage}
                  />
                </div>
              : <div>
                  <p>No results found.</p>
                </div>
            }
          </div>
        </div>
      </div>
    </div>
  );
};
