import React from 'react';
import { ButtonGroup, Button } from "@mui/material";

export const Pagination: React.FC<{
  currentPage: number,
  itemsPerPage: number,
  totalItems: number,
  handlePageChange: (pageNumber: number) => void;
}> = ({
  currentPage,
  itemsPerPage,
  totalItems,
  handlePageChange,
}) => {
  const pagesArray: number[] = Array(Math.ceil(totalItems / itemsPerPage)).fill(0).map((_, index) => index + 1);
  const pagesBegin = currentPage - 4 > -1 ? currentPage - 4 : currentPage - currentPage;
  const pagesEnd = currentPage + 3 < pagesArray.length ? currentPage + 3 : pagesArray.length
  const pagesArrayReduced = pagesArray.slice(pagesBegin, pagesEnd);

  return (
    <div
      className='pagination-container'
      style={{
        position: "relative",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <ButtonGroup variant="outlined" aria-label="outlined primary button group">
        {pagesArray.length < 11 ? pagesArray.map(pg => <Button
                                key={pg}
                                onClick={() => handlePageChange(pg)}
                                variant={currentPage === pg ? 'contained' : 'outlined'}
                              >
                                {pg}
                              </Button>
        ) : (
            <>
              {
                currentPage !== 1 && (<Button
                  onClick={() => handlePageChange(pagesArray[0])}
                  variant={currentPage === pagesArray[0] ? 'contained' : 'outlined'}
                >
                  First
                </Button>)
              }
              {
                pagesArrayReduced.map(pg => <Button
                  key={pg}
                  onClick={() => handlePageChange(pg)}
                  variant={currentPage === pg ? 'contained' : 'outlined'}
                >
                  {pg}
                </Button>
                )
              }
              {
                currentPage !== pagesArray.length &&
                <Button
                  onClick={() => handlePageChange(pagesArray[pagesArray.length - 1])}
                  variant={currentPage === pagesArray[pagesArray.length - 1] ? 'contained' : 'outlined'}
                >
                  Last
                </Button>
              }
            </>
          )
        }
      </ButtonGroup>
      <div
        style={{
          position: "absolute",
          right: "76px",
        }}
      >
        <p>{totalItems < itemsPerPage ? totalItems : itemsPerPage} items of {totalItems}</p>
      </div>
    </div>
  );
};
