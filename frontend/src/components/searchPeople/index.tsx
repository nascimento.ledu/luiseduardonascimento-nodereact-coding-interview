import React, { useEffect, useState } from 'react';
import { TextField } from '@material-ui/core';
import { useDebounce } from 'use-debounce';

export const SearchPeople: React.FC<{
  getUsersByFilter: (searchValueDebounced: string, page: string) => void,
  handleGetAllUsers: (page: string) => void
}> = (
  {getUsersByFilter, handleGetAllUsers}
  ) => {
  const [searchText, setSearchText] = useState<string>('');
  const [searchValueDebounced] = useDebounce(searchText, 1000);
  const [inputHasFocus, setInputHasFocus] = useState<boolean>(false);

  useEffect(() => {
    if (searchValueDebounced) {
      getUsersByFilter(searchValueDebounced, '1');
    } else if (inputHasFocus) {
      handleGetAllUsers('1');
    }
  }, [searchValueDebounced])

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
      }}
    >
      <TextField
        id="outlined-basic"
        label="Search"
        variant="outlined"
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
        placeholder="Title, Gender or Company"
        onFocus={() => setInputHasFocus(true)}
        onBlur={() => setInputHasFocus(false)}

      />

      <hr
        style={{
          width: "100%"
        }}
      />
    </div>
  );
};
