import axios from "axios";
import { IUserProps, IApiResponse } from "../dtos/user.dto";

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers(queryParams?: string): Promise<IApiResponse> {
    return (await axios.get(`${this.baseUrl}/people/all${queryParams || ''}`, {})).data;
  }

  async getByFilter(queryParams: string): Promise<IApiResponse> {
    return (await axios.get(`${this.baseUrl}/people/search${queryParams}`, {})).data;
  }
}
