import people_data from '../data/people_data.json';

interface People {
    id: number;
    avatar: string;
    first_name: string;
    last_name: string;
    email: string;
    gender: string;
    title?: string | null;
    company?: string;
}

interface ResultData {
    users: People[] | [];
    page: Number;
    totalItems: Number;
}

export class PeopleProcessing {
    getById(id: number): ResultData {
        const user: People[] | [] = people_data.filter((p) => p.id === id);
        return {users: user, page: 1, totalItems: 1};
    }

    getAll({
        queryPage,
        queryItemsPerPage,
    }: {queryPage: number, queryItemsPerPage: number}): ResultData {
        let peopleArray = people_data.slice((queryPage - 1) * queryItemsPerPage, (queryPage) * queryItemsPerPage);

        return {users: peopleArray, page: queryPage, totalItems: people_data.length};
    }

    filter({
        searchTerm,
        queryPage,
        queryItemsPerPage,
    }: {
        searchTerm: string,
        queryPage: number,
        queryItemsPerPage: number
    }): ResultData {
        let peopleArray: People[] | [] = [];

        if (searchTerm) {
            peopleArray = people_data.filter(
                (people) => people.title?.toLocaleLowerCase() === searchTerm.toLocaleLowerCase()
                || people.gender?.toLocaleLowerCase() === searchTerm.toLocaleLowerCase()
                || people.company?.toLocaleLowerCase() === searchTerm.toLocaleLowerCase()
            );
        }

        const countItems = peopleArray.length;

        peopleArray = peopleArray.slice((queryPage - 1) * queryItemsPerPage, (queryPage) * queryItemsPerPage);

        return {users: peopleArray, page: queryPage, totalItems: countItems};
    }
}
