import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param,
    QueryParam
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get('/all')
    getAllPeople(
        @QueryParam("page") page?: string,
        @QueryParam("items_per_page") itemsPerPage?: string,
    ) {
        const queryPage = Number(page) || 1;
        const queryItemsPerPage = Number(itemsPerPage) || 12;

        const people = peopleProcessing.getAll({queryPage, queryItemsPerPage});

        if (!people.users) {
            throw new NotFoundError('No people found');
        }

        return {
            data: people,
        };
    }

    @HttpCode(200)
    @Get('/search')
    getByFilter(
        @QueryParam("search_term") searchTerm: string,
        @QueryParam("page") page?: string,
        @QueryParam("items_per_page") itemsPerPage?: string,
    ) {
        const queryPage = Number(page) || 1;
        const queryItemsPerPage = Number(itemsPerPage) || 12;

        const person = peopleProcessing.filter({searchTerm, queryPage, queryItemsPerPage});

        if (!person.users) {
            throw new NotFoundError(`No person found`);
        }

        return {
            data: person,
        };
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person.users) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }
}
